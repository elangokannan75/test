#!/bin/bash -xvf

#Env variables

u="ewb.jenkins"
p="RJ4ddUQv"
irnusers="irnuser1"
tenantID="1"
version="V2"
IP_Port_PrintAPI="127.0.0.1:9000"
IP_Port_TransferAPI="127.0.0.1:9001"
IP_Port_QueueAPI="127.0.0.1:9002"
DB_user="postgres"
DB_password="StrongDBPassword"
DB_host="100.27.15.207"
DB_port="5432"
DB_name="master_db_stg"
ftp_user="FTPUser"
irn_userpass="Changeme@123"

#base URL
svn_baseURL="https://mobifly.xp-dev.com/svn/fatoorah/branches/PwC/irn-services/$version"
home_baseURL="/home/$irnuser/irn-services/$version"
echo $svn_baseURL
echo $home_baseURL



#command Validation
#set -e
# keep track of the last executed command
#trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
#trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

TIME=$(/bin/date +"%m%d%Y_%H_%M")
echo "Setup started in IRN Server at $TIME" >> /home/irn_setup.log
echo "We will do following activites as part of setup 1. User creation 2. Services and Files Folder Creation 3. Services SVN Checkout 4. Cron Setup 5. Application YML Setup 6. Database Setup"



cat <<EOF >/home/irn_setup.log
group irngroup created
EOF


#1. User creation

echo "1. User creation" >> /home/irn_setup.log
#user creation and adding to group
useradd -m $irnuser -p $irn_userpass

#group creation
groupadd irngroup
usermod -a -G irngroup $irnuser
usermod -a -G irngroup $ftp_user

echo "User irnuser created and added to group irngroup" >> /home/irn_setup.log
echo "1. User creation completed" >> /home/irn_setup.log


#2. Services and Files Folder Creation

echo "2. Services and Files Folder Creation" >> /home/irn_setup.log
mkdir $home_baseURL
mkdir $home_baseURL/load $home_baseURL/print $home_baseURL/certificates $home_baseURL/database $home_baseURL/queue $home_baseURL/transfer $home_baseURL/logs $home_baseURL/config $home_baseURL/crons
mkdir /home/$irnuser/irn-files
mkdir /home/$irnuser/irn-files/tenant1
mkdir /home/$irnuser/irn-files/tenant1/archive
mkdir /home/$irnuser/irn-files/tenant1/archive/pdf_files /home/$irnuser/irn-files/tenant1/archive/csv_files /home/$irnuser/irn-files/tenant1/archive/logs
mkdir /home/$irnuser/irn-files/tenant1/pdf_files /home/$irnuser/irn-files/tenant1/sales_invoice /home/$irnuser/irn-files/tenant1/sales_invoice_backup

echo "Directories created" >> /home/irn_setup.log
cd $home_baseURL
echo "Services Folders" >> /home/irn_setup.log
ls >> /home/irn_setup.log

cd /home/$irnuser/irn-files
echo "Irn-Files Folders" >> /home/irn_setup.log
ls >> /home/irn_setup.log

cd /home/$irnuser/irn-files/tenant1
echo "Inside Tenant Folder" >> /home/irn_setup.log
ls >> /home/irn_setup.log

echo "2. Services and Files Folder Creation Completed" >> /home/irn_setup.log

#3. Services SVN Checkout

echo "3. Services SVN Checkout" >> /home/irn_setup.log
svn checkout $svn_baseURL/transfer --username $u --password $p $home_baseURL/transfer >> /home/irn_setup.log
svn checkout $svn_baseURL/load --username $u --password $p $home_baseURL/load >> /home/irn_setup.log
svn checkout $svn_baseURL/print --username $u --password $p $home_baseURL/print >> /home/irn_setup.log
svn checkout $svn_baseURL/queue --username $u --password $p $home_baseURL/queue >> /home/irn_setup.log
svn checkout $svn_baseURL/config --username $u --password $p $home_baseURL/config >> /home/irn_setup.log
svn checkout $svn_baseURL/crons --username $u --password $p $home_baseURL/crons >> /home/irn_setup.log
svn checkout https://mobifly.xp-dev.com/svn/fatoorah/branches/PwC/irn-db/$version --username $u --password $p $home_baseURL/base >> /home/irn_setup.log

echo "Checkout Completed" >> /home/irn_setup.log
cd $home_baseURL/print
echo "Checout Files" >> /home/irn_setup.log
ls -l >> /home/irn_setup.log

echo "3. Services SVN Checkout Completed" >> /home/irn_setup.log


sed  -i "1i Home_baseURL=$home_baseURL" irn_print_cron.sh
sed  -i "1i Home_baseURL=$home_baseURL" irn_queue_cron.sh
sed  -i "1i Home_baseURL=$home_baseURL" irn_transfer_cron.sh

sed  -i "li IP=$IP_Port_PrintAPI" irn_print_cron.sh
sed  -i "li IP=$IP_Port_QueueAPI" irn_queue_cron.sh
sed  -i "1i IP=$IP_Port_TransferAPI" irn_transfer_cron.sh

sed  -i "1i inruser=$irnusers" irn_archive.sh



#4. Cron Setup

echo " 4. Cron Setup" >>/home/irn_setup.log
#setup Cron Job to run the services and check every minute
crontab -e -u root
crontab -l > mycron
#echo new cron into cron file
echo "*/1 * * * * $home_baseURL/crons/irn_print_cron.sh >> /tmp/irn_print_cron.log" >> mycron
echo "*/1 * * * * $home_baseURL/crons/irn_queue_cron.sh >> /tmp/irn_print_cron.log" >> mycron
echo "*/1 * * * * $home_baseURL/crons/irn_transfer_cron.sh >> /tmp/irn_print_cron.log" >> mycron
echo "*/30 2 * * * $home_baseURL/crons/irn_archive_cron.sh >> /tmp/irn_print_cron.log" >> mycron
echo "*/30 2 * * * $home_baseURL/crons/irn_load_cron.sh >> /tmp/irn_load_cron.log" >> mycron
#install new cron file
crontab mycron
rm mycron

echo "Cron setup completed and services started" >> /home/irn_setup.log





#5. Application YML Setup
echo "5. Application YML Setup" >> /home/irn_setup.log
 

cat <<EOF > $home_baseURL/config/application.yml
config:
  user: "$DB_user"
  password: "$DB_password"
  host: "$DB_host"
  port: "$DB_port"
  database: "$DB_name"

folder_location:
  main_file: "/home/$irnuser/irn-files/tenant1/sales_invoice"
  back_file: "/home/$irnuser/irn-files/tenant1/sales_invoice_backup"
  logs_file: "$home_baseURL/logs"

customer_id:
  tenant_1: "$tenantID"
EOF

cat $home_baseURL/config/application.yml >> /home/irn_setup.log

echo "5. Application YML Setup Completed" >> /home/irn_setup.log





# 7. Creating Database



# 8.
#checking file permissions
sudo chown -R $irnuser: /home/$irnuser
sudo chown -R $irngroup: /home/$irnuser/irn-files/tenant1/pdf_files /home/$irnuser/irn-files/tenant1/sales_invoice /home/$irnuser/irn-files/tenant1/sales_invoice_backup
sudo chmod -R 744 /home/$irnuser


TIME=$(/bin/date +"%m%d%Y_%H_%M")
echo "Setup Completed in IRN Server at $TIME" >> /home/irn_setup.log
echo" Thanks for your patience. Kindly sent the log files to the system administartor. Log file can be found at /home/irn_setup.log"
